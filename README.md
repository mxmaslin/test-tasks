Здесь собраны решения предлагаемых IT-компаниями конкурсных и тестовых заданий.

# Конкурсы

## [Wargaming Forge](http://wgforge.wargaming.com/)

- [Алгоритмы и неравные матчи](https://github.com/mxmaslin/test-tasks/tree/master/Wargaming%20Forge%20Task "Алгоритмическое задание конкурса Wargaming Forge")

# Тестовые задания

- [Тест на знание Django ORM](https://github.com/mxmaslin/test-tasks/tree/master/django_orm)
- [Простой RESTful API для социальной сети на FastAPI (WIP)](https://github.com/mxmaslin/test-tasks/tree/master/social)
- [Сервис бронирования](https://github.com/mxmaslin/test-tasks/tree/master/booking)
- [Сервис рассылки сообщений](https://github.com/mxmaslin/test-tasks/tree/master/sender)
- [Загрузка csv в бд](https://github.com/mxmaslin/test-tasks/tree/master/csv2db)
- [Действие после таймаута. Отправка сообщений через websockets](https://github.com/mxmaslin/test-tasks/tree/master/deloitte)
- [Очень простое задание на оценку результата предсказания](https://github.com/mxmaslin/test-tasks/tree/master/evaluate_prediction)
- [Работа с fingerprintjs2](https://github.com/mxmaslin/test-tasks/tree/master/fingerprint)
- [Сумма значений для каждого ключа из словарей, поставляемых генераторами](https://github.com/mxmaslin/test-tasks/tree/master/gen_sum)
- [Генератор pdf с QR-кодами](https://github.com/mxmaslin/test-tasks/tree/master/generate_pdf) 
- [Максимальный массив единиц](https://github.com/mxmaslin/test-tasks/tree/master/max_ones)
- [График изменения в реальном времени лайков, комментариев, репостов](https://github.com/mxmaslin/test-tasks/tree/master/mudakoff)
- [Realtime cчётчик событий определённого типа за интервал времени](https://github.com/mxmaslin/test-tasks/tree/master/nok_counter)
- [Валидация паспорта](https://github.com/mxmaslin/test-tasks/tree/master/passport_validator)
- [REST-приложение для необычной сортировки строк](https://github.com/mxmaslin/test-tasks/tree/master/sort_table)
- [Генератор текста из списков](https://github.com/mxmaslin/test-tasks/tree/master/story-maker-py-for-mxmaslin)
- [Определение эмоциональной окраски твитов](https://github.com/mxmaslin/test-tasks/tree/master/tweets_sentiment)
- [Загрузка/проверка/скачивание файла](https://github.com/mxmaslin/test-tasks/tree/master/uploader)
- [Получение из MongoDB данных и их форматирование](https://github.com/mxmaslin/test-tasks/tree/master/yellowblackwhite-test01_python-9615024cdffa)
